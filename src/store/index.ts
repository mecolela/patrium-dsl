import Vue from 'vue';
import Vuex from 'vuex';
import { FireAuth, FireStore } from '@/services';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    text: ''
  },
  getters: {
    text: ({text}) => text
  },
  actions: {
    async load({commit}) {
      const result = await FireStore.collection('dsl').doc('text').get();
      const data = result.data();
      commit('SET_TEXT', data?.value || '');
    },
    authenticate({dispatch}, {password}) {
      return new Promise((resolve, reject) => {
        FireAuth.signInWithEmailAndPassword('_EMAIL_', password)
          .then(async (res) => {
            if (res.user) {
              resolve(res.user);
            } else {
              reject('An error occurred');
            }
          }).catch(reject);
      });
    },
    async update({commit, dispatch}, {password, value}) {
      const isAuthed = FireAuth.currentUser;

      if (isAuthed) {
        await FireStore.collection('dsl').doc('text').set({value});
        commit('SET_TEXT', value);
      } else {
        dispatch('authenticate', {password})
          .then(() => dispatch('update', {value}))
          .catch(err => {
            throw err;
          });
      }
    }
  },
  mutations: {
    SET_TEXT(state, value) {
      state.text = value;
    }
  }
});
