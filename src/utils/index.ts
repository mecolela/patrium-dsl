export function setupPolyfills() {
  if (!Element.prototype.matches) {
    // @ts-ignore
    Element.prototype.matches = Element.prototype.msMatchesSelector
      || Element.prototype.webkitMatchesSelector;
  }
  if (!Element.prototype.closest) {
    Element.prototype.closest = function (s: string) {
      let el = this;
      do {
        if (Element.prototype.matches.call(el, s)) return el;
        // @ts-ignore-next-line
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);
      return null;
    };
  }
}

export function toFixed(num: number, fixed = 0): string {
  const re = new RegExp(`^-?\\d+(?:.\\d{0,${fixed || -1}})?`);
  return (num.toString().match(re) || [num.toString()])[0];
}

export function slugify(value: string, {separator = '_', trim = true, lowerCase = true, characters = []} = {}): string {
  return (value ? value[lowerCase ? 'toLowerCase' : 'toString']()[trim ? 'trim' : 'toString']()
    .replace(/ /g, separator)[characters.length ? 'replace' : 'toString'](new RegExp(`(${characters.join('|')})`, 'gi'), separator) : '');
}

export function unSlugify(value: string, {separator = '_', join = ' ', upperCase = false} = {}): string {
  return (value ? (value.split(separator)).map((x) => (upperCase ? x.toUpperCase() : capitalize(x))).join(join) : '');
}

export function stringArray(value: any[] | string): string {
  if (Array.isArray(value)) {
    return value.toString().split(',').join(', ');
  }
  return value;
}

/* tslint:disable:no-bitwise */
export function randomKey(length = 8): string {
  return ('x'.repeat(length)).replace(/[x]/g, () => ((Math.random() * 16) | 0).toString(16));
}

export function objectIdGen(): string {
  const t = (new Date().getTime() / 1000 | 0).toString(16);
  return t + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
}

/* tslint:enable:no-bitwise */

export function randomInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomColor() {
  let color = '#';
  const hex = '0123456789ABCDEF';
  for (let i = 0; i < 6; i++) {
    color += hex[randomInt(0, 15)];
  }
  return color;
}

export function sumBy(array: any[], iterator: (item: any) => any): number {
  let val = 0;
  array.forEach((item) => val += iterator(item));
  return val;
}

export function capitalize(str: string): string {
  return str ? str.split(' ').map((x) => x.charAt(0).toUpperCase() + (x.toLowerCase()).slice(1)).join(' ') : '';
}

export function toJsCase(str: string): string {
  const buildStr: string[] = [];
  str.trim().split(' ').forEach((slice, ix) => buildStr.push(ix === 0 ? slice.toLowerCase() : capitalize(slice)));
  return buildStr.join('');
}

const ObjectType = (obj: any) => Object.prototype.toString.call(obj);

export function isEmpty(obj: any) {
  if (typeof obj === 'undefined') {
    return true;
  }

  if (!obj) {
    return true;
  }

  if (Array.isArray(obj) && obj.length === 0) {
    return true;
  }

  if (ObjectType(obj) === '[object Object]') {
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        return false;
      }
    }
  } else {
    // By this point it's a valid string / number
    return false;
  }

  return true;
}

export function $parse(string: string | null): any {
  try {
    return JSON.parse(string as string);
  } catch (e) {
    return null;
  }
}

export function $clone(obj: any, useJson = false): any {
  return useJson || Array.isArray(obj) ? JSON.parse(JSON.stringify(obj)) : ({...obj});
}

export function arrayToObj(array: any[], key: string, value: string): any {
  const temp = {};
  // @ts-ignore
  array.forEach((val) => temp[val[key]] = val[value]);
  return temp;
}

const DEBOUNCE_MILLIS = 100;

export function keyedDebounce(callback: Function, millis: number = DEBOUNCE_MILLIS): Function {
  let timeout;
  let results: any = {};

  return function (key: string, ...args: any[]) {
    results[key] = args;

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      if (!Object.keys(results).length) {
        return;
      }

      callback(results);
      results = {};
    }, millis);
  };
}

export function debounce(callback: Function, millis: number = DEBOUNCE_MILLIS): Function {
  // For regular debounce, just use a keyed debounce with a fixed key
  return keyedDebounce((results: any) => {
    // eslint-disable-next-line prefer-spread
    callback.apply(null, results.__key__);
  }, millis).bind(null, '__key__');
}
