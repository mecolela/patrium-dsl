import Vue, { PluginObject } from 'vue';

export const EventBus = new Vue();

interface IEvBusOption {
  propName?: string;
}

export const EventBusPlugin: PluginObject<IEvBusOption> = {
  install(Vue, options: IEvBusOption = { propName: '$bus' }) {
    if (!Vue.prototype.$bus) {
      Object.defineProperties(Vue.prototype, { [options.propName as string]: { get: () => EventBus } });
    }
  }
};
