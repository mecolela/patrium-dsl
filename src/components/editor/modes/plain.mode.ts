import CodeMirror, { Mode, StringStream } from 'codemirror';

import 'codemirror/addon/mode/overlay';
import 'codemirror/addon/display/placeholder';

CodeMirror.defineMode('plain', (config, parserConfig) => {
  const baseMode = CodeMirror.getMode(config, parserConfig.baseMode || 'text/plain');
  return CodeMirror.overlayMode(baseMode, _plainMode(), false);
});

function _plainMode(): Mode<any> {
  let ticker = 0;
  const regexVar = /{{\s*([^ }{]+)\s*[^}{]*\s*}}/i;

  return {
    token(stream: StringStream) {
      // This makes sure that adjacent tags still have unique types
      ticker -= 1;

      if (stream.match(regexVar, true)) {
        return `plain-var ${ticker}`;
      }

      while (stream.next() != null) {
        if (stream.match(regexVar, false)) break;
      }

      return null;
    }
  };
}
