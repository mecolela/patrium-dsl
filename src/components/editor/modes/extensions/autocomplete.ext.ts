import CodeMirror from 'codemirror';
import { replaceWithSurround } from '../../utils';

const NAME_MATCH_FLEXIBLE = /[\w.[\-/]+$/;
const NAME_MATCH = /[\w.[\-_]+$/;
const COMPLETE_AFTER_WORD = /{{\s*[\w.\][\-_]+/;
const COMPLETE_AFTER_CURLIES = /{/;
const COMPLETION_CLOSE_KEYS = /[}|-]/;
const MAX_HINT_LOOK_BACK = 100;
const TYPE_VAR = 'variable';
const TYPE_ACT = 'action';
const MAX_VARIABLES = -1;

CodeMirror.defineExtension('isHintDropdownActive', function () {
  return (
    this.state.completionActive
    && this.state.completionActive.data
    && this.state.completionActive.data.list
    && this.state.completionActive.data.list.length
  );
});

CodeMirror.defineOption('envAutocomplete', null, (cm, options: any) => {
  if (!options) {
    return;
  }

  async function completeAfter(cmAfter, fn, showAllOnNoMatch = false) {
    // Bail early if didn't match the callback test
    if (fn && !fn()) {
      return;
    }

    if (!cmAfter.hasFocus()) {
      return;
    }

    // Bail early if completions are showing already
    if (cmAfter.isHintDropdownActive()) {
      return;
    }

    let hintsContainer = document.querySelector('#render #hints');
    if (!hintsContainer) {
      const el = document.createElement('div');
      el.id = 'hints';
      document.querySelector('#render')?.appendChild(el);
      hintsContainer = el;
    }

    const variables = options.getVariables ? await options.getVariables() : [{name: 'test', value: 'sample'}];
    const actions = options.getActions ? await options.getActions() : [{name: 'demo', value: 'trial'}];

    // Actually show the hint
    cmAfter.showHint({
      actions: actions || [],
      variables: variables || [],
      showAllOnNoMatch,

      // Codemirror native options
      hint,
      container: hintsContainer,
      closeCharacters: COMPLETION_CLOSE_KEYS,
      completeSingle: false,
      extraKeys: {
        Esc: (cm0, widget) => {
          widget.close();
          return CodeMirror.Pass;
        }
      }

      // Good for debugging
      ,closeOnUnfocus: false
    });
  }

  function completeIfInVariableName(cm1) {
    completeAfter(cm1, () => {
      const cur = cm1.getCursor();
      const pos = CodeMirror.Pos(cur.line, cur.ch - MAX_HINT_LOOK_BACK);
      const range = cm1.getRange(pos, cur);
      return range.match(COMPLETE_AFTER_WORD);
    });

    return CodeMirror.Pass;
  }

  function completeIfAfterVarOpen(cm2) {
    completeAfter(cm2, () => {
      const cur = cm2.getCursor();
      const pos = CodeMirror.Pos(cur.line, cur.ch - 2);
      const range = cm2.getRange(pos, cur);
      return range.match(COMPLETE_AFTER_CURLIES);
    },
    true);

    return CodeMirror.Pass;
  }

  function completeForce(cm3) {
    completeAfter(cm3, null, true);
    return CodeMirror.Pass;
  }

  let keydownDebounce;

  cm.on('keydown', async (cm4, e) => {
    // Close autocomplete on Escape if it's open
    if (cm4.isHintDropdownActive() && e.key === 'Escape') {
      if (!cm4.state.completionActive) {
        return;
      }

      e.preventDefault();
      e.stopPropagation();

      cm4.state.completionActive.close();
    }

    // Only operate on one-letter keys. This will filter out
    // any special keys (Backspace, Enter, etc)
    if (e.metaKey || e.ctrlKey || e.altKey || e.key.length > 1) {
      return;
    }

    clearTimeout(keydownDebounce);

    const autocompleteDelay = 100;

    if (autocompleteDelay > 0) {
      keydownDebounce = setTimeout(() => {
        completeIfInVariableName(cm4);
      }, autocompleteDelay);
    }
  });

  // Clear timeout if we already closed the completion
  cm.on('endCompletion', () => {
    clearTimeout(keydownDebounce);
  });

  // Remove keymap if we're already added it
  cm.removeKeyMap('autocomplete-keymap');

  // Add keymap
  cm.addKeyMap({
    name: 'autocomplete-keymap',
    'Ctrl-Space': completeForce, // Force autocomplete on hotkey
    '\'{\'': completeIfAfterVarOpen // Induce autocomplete at the variable opener
  });
});

/**
 * Function to retrieve the list items
 * @param cm
 * @param options
 * @returns Promise<{list: Array, from, to}>
 */
function hint(cm: any, options: any): { list: any[]; from: CodeMirror.Position; to: CodeMirror.Position } {
  // Add type to all things (except constants, which need to convert to an object)
  const variablesToMatch = (options.variables || []).map((v) => ({ ...v, type: TYPE_VAR }));
  const actionsToMatch = (options.actions || []).map((v) => ({ ...v, type: TYPE_ACT }));
  const itemsToMatch = [...variablesToMatch, ...actionsToMatch];

  // Get the text from the cursor back
  const cur = cm.getCursor();
  const pos = CodeMirror.Pos(cur.line, cur.ch - MAX_HINT_LOOK_BACK);
  const previousText = cm.getRange(pos, cur);

  // Define fallback segment to match everything or nothing
  const fallbackSegment = options.showAllOnNoMatch ? '' : '__will_not_match_anything__';

  // See if we're completing a variable name
  const nameMatch = previousText.match(NAME_MATCH);
  const nameMatchLong = previousText.match(NAME_MATCH_FLEXIBLE);
  const nameSegment = nameMatch ? nameMatch[0] : fallbackSegment;
  const nameSegmentLong = nameMatchLong ? nameMatchLong[0] : fallbackSegment;

  // Actually try to match the list of things
  const lowPriorityMatches: any[] = [];
  const highPriorityMatches: any[] = [];

  matchSegments(itemsToMatch, nameSegment, MAX_VARIABLES).forEach((m) => lowPriorityMatches.push(m));
  matchSegments(itemsToMatch, nameSegmentLong, MAX_VARIABLES).forEach((m) => highPriorityMatches.push(m));

  const matches = [...highPriorityMatches, ...lowPriorityMatches];

  // Autocomplete from longest matched segment
  const segment = highPriorityMatches.length ? nameSegmentLong : nameSegment;

  const uniqueMatches = matches.reduce(
    (arr, v) => (arr.find((a: any) => a.text === v.text) ? arr : [...arr, v]),
    [] // Default value
  );

  return {
    list: uniqueMatches,
    from: CodeMirror.Pos(cur.line, cur.ch - segment.length),
    to: CodeMirror.Pos(cur.line, cur.ch)
  };
}

/**
 * Replace the text in the editor when a hint is selected.
 * This also makes sure there is whitespace surrounding it
 * @param cm
 * @param self
 * @param data
 */
async function replaceHintMatch(cm, self, data) {
  if (typeof data.text === 'function') {
    data.text = await data.text();
  }

  const RETROSPECT_CHARS = 25;
  const cur = cm.getCursor();
  const to = CodeMirror.Pos(cur.line, cur.ch);
  let from = CodeMirror.Pos(cur.line, cur.ch - data.segment.length);

  const prevStart = CodeMirror.Pos(from.line, from.ch - RETROSPECT_CHARS);
  const prevChars = cm.getRange(prevStart, from);

  const nextEnd = CodeMirror.Pos(to.line, to.ch + 10);
  const nextChars = cm.getRange(to, nextEnd);

  const spaced = prevChars.match(/{{\s*[\w]{1,}[\s]{1,}[\w\s]*$/);
  // TODO: Review space replacing algorithm
  if (spaced) {
    from = {
      line: from.line,
      ch: from.ch - (RETROSPECT_CHARS - spaced.index) + 2 // 2 accounts for curly braces
    };
  }

  let prefix = '';
  let suffix = '';

  if (!prevChars.match(/{{[^}]*$/)) {
    prefix = '{{ '; // If no closer before
  } else if (prevChars.match(/{{$/)) {
    prefix = ' '; // If no space after opener
  }

  if (!nextChars.match(/^\s*}}/)) {
    suffix = ' }}'; // If no closer after
  } else if (nextChars.match(/^}}/)) {
    suffix = ' '; // If no space before closer
  }

  cm.replaceRange(`${prefix}${data.text}${suffix}`, from, to);
}

/**
 * Match against a list of things
 * @param listOfThings - Can be list of strings or list of {name, value}
 * @param segment - segment to match against
 * @param limit
 * @returns Array
 */
function matchSegments(listOfThings: any[], segment: string, limit: any = -1) {
  if (!Array.isArray(listOfThings)) {
    console.warn('Autocomplete received items in non-list form', listOfThings);
    return [];
  }

  const matches: any[] = [];
  for (const t of listOfThings) {
    const name = typeof t === 'string' ? t : t.name;
    const value = typeof t === 'string' ? '' : t.value;
    const displayName = t.displayName || name;

    // Generate the value we'll fill the space with
    const defaultFill = t.name;

    const matchSegment = segment.toLowerCase();
    const matchName = displayName.toLowerCase();

    // Throw away things that don't match
    if (!matchName.includes(matchSegment)) {
      continue;
    }

    // Value shown in the hint
    let { displayValue } = t;
    if (typeof displayValue !== 'string' && typeof value !== 'function') {
      displayValue = JSON.stringify(value);
    }

    matches.push({
      // Custom keys
      type: t.type,
      segment,
      displayValue,
      comment: value,
      score: name.length, // In case we want to sort by this

      // CodeMirror
      text: defaultFill,
      displayText: displayName,
      render: renderHintMatch,
      hint: replaceHintMatch
    });
  }

  if (limit >= 0) {
    return matches.slice(0, limit);
  }

  return matches;
}

/**
 * Render the autocomplete list entry
 * @param li
 * @param self
 * @param data
 */
function renderHintMatch(li, self, data) {
  // Bold the matched text
  const { displayText, segment } = data;
  let text = displayText;

  if (data.type === TYPE_ACT) {
    text = displayText.replace(/_/g, ' ');
  }

  let markedName = displayText;

  if (segment) {
    markedName = replaceWithSurround(text, segment, '<strong>', '</strong>');
  }

  li.className += ` type-${data.type}`;
  li.innerHTML = `<div class="name">${markedName}</div>`;
}
