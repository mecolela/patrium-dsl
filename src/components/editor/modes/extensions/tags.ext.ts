import CodeMirror from 'codemirror';
import { debounce, isEmpty } from '@/utils';
import { extractCtx } from '../../utils';
import { EventBus } from '@/utils/event-bus.plugin';

interface IExtContext {
  data: { [key: string]: string };
}

CodeMirror.defineExtension('enablePatriumOverlay', function (context: IExtContext, isVariableUncovered = false) {
  const refreshFn = _highlightPatriumTags.bind(this, context, isVariableUncovered);
  const debouncedRefreshFn = debounce(refreshFn);

  this.on('change', (cm: any, change: any) => {
    const origin = change.origin || 'unknown';
    if (!origin.match(/^[+*]/)) {
      // Refresh immediately on non-joinable events
      // (cut, paste, autocomplete; as opposed to +input, +delete)
      refreshFn();
    } else {
      // Debounce all joinable events
      debouncedRefreshFn();
    }
  });

  this.on('cursorActivity', debouncedRefreshFn);

  this.on('viewportChange', debouncedRefreshFn);

  // Trigger once right away to snappy perf
  refreshFn();
});

async function _highlightPatriumTags(context: IExtContext, isVariableUncovered: boolean) {
  const activeMarks: any[] = [];
  const doc = this.getDoc();

  // Only mark up Nunjucks tokens that are in the viewport
  const vp = this.getViewport();
  for (let lineNo = vp.from; lineNo < vp.to; lineNo++) {
    const line = this.getLineTokens(lineNo);
    const tokens = line.filter(({type}) => type && type.indexOf('patrium') >= 0);

    // Aggregate same tokens
    const newTokens: any[] = [];
    let currTok: any = null;
    for (let i = 0; i < tokens.length; i++) {
      const nextTok = tokens[i];

      if (currTok && currTok.type === nextTok.type && currTok.end === nextTok.start) {
        currTok.end = nextTok.end;
        currTok.string += nextTok.string;
      } else if (currTok) {
        newTokens.push(currTok);
        currTok = null;
      }

      if (!currTok) {
        currTok = {...nextTok};
      }
    }

    // Push the last one if we're done
    if (currTok) {
      newTokens.push(currTok);
    }

    for (const tok of newTokens) {
      const start = {line: lineNo, ch: tok.start};
      const end = {line: lineNo, ch: tok.end};
      const cursor = doc.getCursor();
      const isSameLine = cursor.line === lineNo;
      const isCursorInToken = isSameLine && cursor.ch > tok.start && cursor.ch < tok.end;
      const isFocused = this.hasFocus();

      // Show the token again if we're not inside of it.
      if (isFocused && isCursorInToken) {
        continue;
      }

      // See if we already have a mark for this
      let hasOwnMark = false;
      for (const m of doc.findMarks(start, end)) {
        // Only check marks we created
        if (m.__patrium) {
          hasOwnMark = true;
        }

        activeMarks.push(m);
      }

      // Already have a mark for this, so leave it alone
      if (hasOwnMark) {
        continue;
      }

      let el;
      const string = tok.string.replace(/[{}]/g, '').trim();

      if (tok.type.includes('patrium-input')) {
        const ctx = extractCtx(string, true);
        const node = document.createElement('div');

        // input field
        const input = document.createElement('input');
        input.setAttribute('field', ctx.field);
        input.setAttribute('placeholder', ctx.placeholder);

        // overlay div
        const overlay = document.createElement('div');
        overlay.setAttribute('field', ctx.field);
        overlay.classList.add('overlay');

        node.appendChild(input);
        node.appendChild(overlay);
        el = node;
      } else {
        el = document.createElement('span');
      }

      el.className = `patrium-tag ${tok.type}`;
      // el.setAttribute('data-error', 'off');
      el.setAttribute('data-template', tok.string);

      const mark = this.markText(start, end, {
        __patrium: true, // Mark that we created it
        __template: tok.string,
        handleMouseEvents: false,
        replacedWith: el
      });

      (async function () {
        await _updateElementText(
          mark,
          tok.string,
          context,
          isVariableUncovered
        );
      }());

      // Update it every mouseenter because it may generate a new value every time
      el.addEventListener('mouseenter', async () => {
        await _updateElementText(
          mark,
          tok.string,
          context,
          isVariableUncovered
        );
      });

      EventBus.$on('ptm-update', () => {
        _updateElementText(
          mark,
          tok.string,
          context,
          isVariableUncovered
        );
      });

      activeMarks.push(mark);

      el.addEventListener('click', async (ev) => {
        EventBus.$emit('patrium-click', ev);
      });
    }
  }

  // Clear all the marks that we didn't just modify/add
  // For example, adding a {% raw %} tag would need to clear everything it wrapped
  const marksInViewport = doc.findMarks({ch: 0, line: vp.from}, {ch: 0, line: vp.to});

  for (const mark of marksInViewport) {
    // Only check marks we created
    if (!mark.__patrium) {
      continue;
    }

    let inActiveMarks = false;
    for (const activeMark of activeMarks) {
      if (activeMark.id === mark.id) {
        inActiveMarks = true;
      }
    }

    if (!inActiveMarks) {
      mark.clear();
    }
  }
}

async function _updateElementText(mark, text, context: IExtContext, isVariableUncovered = false) {
  const el = mark.replacedWith;
  const str = text.replace(/\\/g, '');
  const strippedStr = str
    .replace(/^{{/, '')
    .replace(/}}$/, '')
    .trim();

  const ctx = extractCtx<any>(strippedStr);
  const ctxObj = context.data;
  const value = ctxObj[ctx.field] || ctx.content || '';

  const id = `ptm${el.classList[el.classList.length - 1]}`;
  el.id = id;
  let show;

  if (!isEmpty(ctx.condition)) {
    const {field, comparator, value} = ctx.condition;

    switch (comparator) {
      case 'IS_EQUAL_TO':
        show = ctxObj[field] === value;
        break;
      default:
        show = true;
    }

    if (show) {
      el.classList.remove('hide');
    } else {
      el.classList.add('hide');
    }
  }

  // set data fields
  el.dataset.id = id;
  el.dataset.type = ctx.type;
  el.dataset.field = ctx.field;

  if (ctx.options) {
    el.dataset.options = ctx.options;
  }

  if (ctx.condition) {
    el.dataset.condition = JSON.stringify(ctx.condition);
  }

  if (el.classList.contains('patrium-input')) {
    const overlay = el.querySelector('.overlay');
    const input = el.querySelector('input');
    overlay.innerHTML = value;
    input.style.width = overlay.getBoundingClientRect().width || 200 + 'px';
  } else {
    // set inner content
    if (Array.isArray(value)) {
      el.innerHTML = value[0];
    } else {
      el.innerHTML = value;
    }
  }

  mark.changed();
}
