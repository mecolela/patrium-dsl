import CodeMirror, { Mode, StringStream } from 'codemirror';

import 'codemirror/addon/mode/overlay';
import 'codemirror/addon/display/placeholder';
import 'codemirror/addon/hint/show-hint';
import './extensions/tags.ext';
import './extensions/autocomplete.ext';
import { extractCtx } from '@/components/editor/utils';
import { EventBus } from '@/utils/event-bus.plugin';

interface IContextDefinition {
  type: 'select' | 'text' | 'input';
  field?: string;
  options: string[]
  placeholder?: string;
  condition?: {
    field: string;
    comparator: string;
    value: string;
  };
}

CodeMirror.defineMode('patrium', (config, parserConfig) => {
  const baseMode = CodeMirror.getMode(config, parserConfig.baseMode || 'text/plain');
  return CodeMirror.overlayMode(baseMode, _patriumMode(), false);
});

function _patriumMode(): Mode<any> {
  let ticker = 0;
  const regexVar = /{{\s*([^ }{]+)\s*[^}{]*\s*}}/i;

  return {
    startState() {
      return {found: []}
    },
    token(stream: StringStream, state) {
      // This makes sure that adjacent tags still have unique types
      ticker -= 1;

      const match = stream.match(regexVar, true);

      if (match) {
        const val = match[0].replace(/[{}]/g, '').trim();
        const ctx = extractCtx<IContextDefinition>(val);

        if (ctx.field && !state.found.includes(ctx.field)) {
          state.found.push(ctx.field);
          EventBus.$emit('patrium-field', ctx);
        }

        let className = '';

        switch (ctx.type) {
          case 'select':
            className = 'patrium-select'
            break;
          case 'text':
            className = 'patrium-text'
            break;
          case 'input':
            className = 'patrium-input'
            break;
        }

        return `patrium-var${ticker} ${className}`;
      }

      while (stream.next() != null) {
        if (stream.match(regexVar, false)) break;
      }

      return null;
    }
  };
}
