import { arrayToObj, slugify } from '@/utils';

const ESCAPE_REGEX_MATCH = /[-[\]/{}()*+?.\\^$|]/g;

export function escapeRegex(str: string): string {
  return str.replace(ESCAPE_REGEX_MATCH, '\\$&');
}

export function extractCtx<T = any>(str: string, rawOpts: boolean = false): T {
  const obj: any = {};
  const parts = str.split(';').map(x => x.trim()).filter(Boolean);

  // Split sections
  parts.forEach(part => {
    // Get  key value pairs
    let [key, value = true]: any[] = part.split('=').map(x => x.trim());

    if (!rawOpts && typeof value === 'string'
      && (['options'].includes(key) && (value.includes('/') || value.includes('|')))
    ) {
      // support groups as a default
      const groups = value.split('|').map(x => x.trim());

      const subGroupRx = /\[\s*[\w\s]*\]\s*\(\s*[,\w\s]*\)/gi;

      value = groups.map(val => {
        const options = val.split('/').map(x => x.trim());

        return options.map((opt) => {
          if (opt.match(subGroupRx)) {
            const [main, subList] = opt.replace(/]\s*\(/, '||||')
              .split('||||').map(x => x.replace(/[\[)]/, ''));

            // return (options as any)[ix] = [main.trim(), subList.split(',').map(x => x.trim())];
            return [main.trim(), subList.split(',').map(x => x.trim())];
          } else {
            return opt;
          }
        });
      });
    }

    if (!rawOpts && key === 'condition') {
      const [field, comparator, result] = value.replace(/[\[\]]/g, '').split(',').map(x => x.trim());
      value = {field, comparator, value: result};
    }

    obj[key] = value;
  });

  return obj;
}

export function render(str: string, data = {}) {
  data = {...data};
  return Object.entries(data).reduce((res, [key, val]) => {
    switch (typeof val) {
      case 'string':
      case 'object':
        val = `"${val}"`;
        break;
      default:
        break;
    }
    return res.replace(new RegExp(`{{\\s*${key}\\s*}}`, 'g'), val as string);
  }, str);
}

export function prepContext(obj) {
  return arrayToObj(Object.keys(obj).map((key) => ({
    label: slugify(key),
    value: obj[key]
  })), 'label', 'value');
}

/**
 * Replace all occurrences of string
 * @param text
 * @param find
 * @param prefix
 * @param suffix
 * @returns string
 */
export function replaceWithSurround(text, find, prefix, suffix) {
  const escapedString = escapeRegex(find);
  const re = new RegExp(escapedString, 'gi');
  return text.replace(re, (matched) => prefix + matched + suffix);
}
