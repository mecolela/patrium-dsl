import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

let config = {};

const app = firebase.initializeApp(config);

export const FireAuth = app.auth();
export const FireStore = app.firestore();

