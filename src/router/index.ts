import Vue from 'vue';
import Store from '@/store';
import Home from '../views/Home.vue';
import VueRouter, { Route, RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to: Route, from: Route, next: any) => {
  Store.dispatch('load')
    .then((dest) => next())
    .catch((err: { code: number; message: string }) => console.error(err.message || err));
});

export default router;
