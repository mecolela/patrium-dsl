# [Patrium DSL](https://patrium.cole.la)
> Domain specific language to create live forms from plain text documents

### Setting up

```bash
# install dependencies
yarn install

# run server
yarn serve
```
